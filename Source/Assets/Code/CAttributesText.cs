﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CAttributesText : MonoBehaviour
{
    CGameManager GameManager;
    Text GUIText;
    public int i_Attribute = 0;

	// Use this for initialization
	void Start()
    {
        if (Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();

        GUIText = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update()
    {
        switch(i_Attribute)
        {
            case 0:
                GUIText.text = GameManager.GameData.PlayerDat.i_SkilledDamage.ToString();
                break;
                
            case 1:
                GUIText.text = GameManager.GameData.PlayerDat.i_SkilledSpeed.ToString();
                break;
                
            case 2:
                GUIText.text = GameManager.GameData.PlayerDat.i_SkilledHealth.ToString();
                break;

            default:
                GUIText.text = GameManager.GameData.PlayerDat.i_SkilledDamage.ToString();
                break;
        }
	}
}
