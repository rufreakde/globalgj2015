﻿using UnityEngine;
using System.Collections;

public class CGoldCollision : MonoBehaviour
{
    void OnTriggerEnter(Collider _target)
    {
        if (_target.GetType() == typeof(CapsuleCollider) && _target.transform.tag == "Enemy")
        {
            //Debug.Log("steal at " + transform.position);
            CDefaultEnemy Enemy = _target.transform.GetComponent<CDefaultEnemy>();

            if (!Enemy.b_IstoleSomething)
            {
            
                //Debug.Log("steal at " + transform.position);
                Enemy.Steal();
                Enemy.b_IstoleSomething = true;
            }
        }
    }
}
