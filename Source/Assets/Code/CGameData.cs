﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CGameData : MonoBehaviour
{
    [System.Serializable]
    public struct PlayerData
    {
        public int i_Health;
        public int i_HealthMax;
        public int i_Damage;
        public float f_Speed;
        public int i_Bombs;
        public int i_MaxBombs;
        public bool b_IsDead;
        public int i_HealthAddition;
        public int i_DamageAddition;
        public float f_SpeedAddition;
        public int i_SkilledHealth;
        public int i_SkilledDamage;
        public int i_SkilledSpeed;

        public PlayerData(int _Health)
        {
            i_Health = _Health;
            i_HealthMax = _Health;
            i_Damage = 1;
            f_Speed = 10f;
            i_Bombs = 3;
            i_MaxBombs = 5;
            b_IsDead = false;
            i_HealthAddition = 5;
            i_DamageAddition = 3;
            f_SpeedAddition = 0.1f;
            i_SkilledHealth = 0;
            i_SkilledDamage = 0;
            i_SkilledSpeed = 0;

        }
    };

    [System.Serializable]
    public struct GameData
    {
        public int i_Difficulty;
        public int i_MaximumGold;
        public int i_Gold;
        public int i_EnemyCount;

        public GameData(int _Difficulty)
        {
            i_Difficulty = _Difficulty;
            i_MaximumGold = 1000;
            i_Gold = 1000;
            i_EnemyCount = 3;
        }
    };
    public PlayerData PlayerDat = new PlayerData(3);
    public GameData GameDat = new GameData(0);
}
