﻿using UnityEngine;
using System.Collections;


public enum PlayerUpgrades
{ 
    Health,
    Damage,
    Speed
}

public class CPlayer : MonoBehaviour {


    public CGameManager GameManager;
    public GameObject BombPrefab;
    public GameObject DamagePrefab;
    public SkinnedMeshRenderer PiratMeshRenderer;
    public AudioClip Attack_1, Attack_2;
    protected Animator a_MyAnimator;
    protected AudioSource a_MyAudioSource;

    public string s_MeshName = "Pirat";
    public Color c_DamageColor = new Color(1.0f, 0.1f, 0.1f, 0.75f);
    public float f_DamageBlinkTime = 0.8f;
    public float f_DamageBlinkSpeed = 0.2f;
    public float f_BombPlantingAnimationTime = 1.5f;
    public float f_AttackingAnimationTime = 0.6f;

    public Transform objectToOrbit;             //Object To Orbit
    Vector3 orbitAxis = Vector3.up;             //Which vector to use for Orbit
    public float orbitRoationSpeed = 45.0f;     //Speed Of Rotation arround object
    public float UpDownSpeed = 2f;
    public float PlayerSpeedFactor = 0.85f;
    public float distanceMax = 18f;
    public float distanceMin = 4f;
    public float f_Tolerance = 0.4f;
    public float f_StopTolerance = 0.5f;
    protected bool b_isPlanting = false;
    protected bool b_isAttacking = false;

    public float walkDeaccelerationVolx = 0.4f;
    public float walkDeacceleration = 0.1f;
    public float walkDeaccelerationVolz = 0.4f;

    public Transform calcTransform;

    private Rigidbody thisRigidbody;
    private Vector3 velocityRef;

 
     void Start() 
     {    
         thisRigidbody = transform.GetComponent<Rigidbody>();
         a_MyAnimator = transform.FindChild("Pirat").GetComponent<Animator>();
         a_MyAudioSource = this.transform.GetComponent<AudioSource>();
     }
 
     void FixedUpdate() 
     {
         if(!GameManager.GameData.PlayerDat.b_IsDead && !GameManager.b_Pause)
         {
            AmIDead();
            Move();
            Attack();
            PlantBomb();
         }

         GameManager.CheckGameStatus();
     }

    void Move()
     {
         float distance = Vector3.Distance(new Vector3(calcTransform.position.x, 0.0f, calcTransform.position.z), new Vector3(objectToOrbit.position.x, 0.0f, objectToOrbit.position.z));
         //Rotation movement

         if (!b_isPlanting && !b_isAttacking) //gehen
         {
             if (Input.GetAxis("Horizontal") != 0)
             {
                 calcTransform.RotateAround(objectToOrbit.position, orbitAxis, (orbitRoationSpeed * -Input.GetAxis("Horizontal")) * GameManager.f_CustomDeltaTime);
             }

             if (Input.GetAxis("Vertical") > 0)
             {
                 if (distance >= distanceMin)
                 {
                     calcTransform.position += ((objectToOrbit.position - calcTransform.position).normalized * GameManager.f_CustomDeltaTime * UpDownSpeed) * Input.GetAxis("Vertical");
                 }
             }
             else if (Input.GetAxis("Vertical") < 0)
             {
                 if (distance <= distanceMax)
                 {
                     calcTransform.position += ((calcTransform.position - objectToOrbit.position).normalized * GameManager.f_CustomDeltaTime * UpDownSpeed) * -Input.GetAxis("Vertical");
                 }
             }

             a_MyAnimator.SetBool("Walk", true);
             thisRigidbody.velocity = ((new Vector3(calcTransform.position.x, 0.0f, calcTransform.position.z) - new Vector3(thisRigidbody.position.x, 0.0f, thisRigidbody.position.z)).normalized * GameManager.GameData.PlayerDat.f_Speed * PlayerSpeedFactor);
         }
        if (Vector3.Distance(new Vector3(thisRigidbody.position.x, 0.0f, thisRigidbody.position.z), new Vector3(calcTransform.position.x, 0.0f, calcTransform.position.z)) <= f_Tolerance)
         {
             thisRigidbody.velocity = Vector3.zero;
             a_MyAnimator.SetBool("Walk", false);
         }

         transform.LookAt(new Vector3(calcTransform.position.x, transform.position.y, calcTransform.position.z));
    }

    void OnDrawGizmos()
     {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(objectToOrbit.position, distanceMin);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(objectToOrbit.position, distanceMax);

        Gizmos.color = Color.magenta;
        Gizmos.DrawCube(calcTransform.position, new Vector3(0.4f, 2.4f, 0.4f));
     }

    void AmIDead()
    {
        if(GameManager.GameData.PlayerDat.i_Health <= 0)
        {
            GameManager.GameData.PlayerDat.b_IsDead = true;
            a_MyAnimator.SetBool("Dead", true);
        }
    }

    void PlantBomb()
    {
        if (Input.GetAxis("Bomb") != 0 && !b_isPlanting)
        {
            if(GameManager.GameData.PlayerDat.i_Bombs > 0)
            {
                a_MyAnimator.SetTrigger("Bomb");
                b_isPlanting = true;
                StartCoroutine(Planting(f_BombPlantingAnimationTime));
            }
        }
    }

    void Attack()
    {
        if (Input.GetAxis("SwingSaber") != 0 && !b_isAttacking)
        {
            //Debug.Log("I am attacking YARRRR!!!");
            a_MyAnimator.SetTrigger("Attack");

            if(Random.Range(0, 2) < 1)
            {
                a_MyAudioSource.clip = Attack_1;
                a_MyAudioSource.Play();
            }
            else
            {
                a_MyAudioSource.clip = Attack_2;
                a_MyAudioSource.Play();
            }

            StartCoroutine(Attacking(f_AttackingAnimationTime));
            b_isAttacking = true;
        }
    }

    public void GetDamage(int _damage)
    {
        StartCoroutine(Blinking(f_DamageBlinkTime));
        GameManager.GameData.PlayerDat.i_Health -= _damage;
    }

    IEnumerator Blinking(float _time)
    {
        float f_counter = _time;
        float f_color = 0.0f;
        bool b_isRed = false;
        Renderer r_temp = PiratMeshRenderer;

        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;
            f_color += GameManager.f_CustomDeltaTime;

            if (f_color >= f_DamageBlinkSpeed)
            {
                if (b_isRed)
                    r_temp.material.SetColor("_Color", c_DamageColor);
                else
                    r_temp.material.SetColor("_Color", Color.white);

                if (b_isRed)
                    b_isRed = false;
                else
                    b_isRed = true;
                f_color = 0.0f;

            }
            yield return new WaitForEndOfFrame();
        }
        r_temp.material.SetColor("_Color", Color.white);
    }

    IEnumerator Planting(float _animationTime)
    {
        float f_counter = _animationTime;

        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;

            //Planting a bomb animation
            yield return new WaitForEndOfFrame();
        }

        Instantiate(BombPrefab, new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z) + transform.forward * 0.4f, Quaternion.identity);
        GameManager.GameData.PlayerDat.i_Bombs--;

        b_isPlanting = false;
    }

    IEnumerator Attacking(float _animationTime)
    {
        float f_counter = _animationTime;


        GameObject temp = Instantiate(DamagePrefab, new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z) + transform.forward, Quaternion.identity) as GameObject;
        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;

            //Planting a bomb animation
            yield return new WaitForEndOfFrame();
        }

        Destroy(temp);
        b_isAttacking = false;
    }
}
