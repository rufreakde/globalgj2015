﻿using UnityEngine;
using System.Collections;


public class CWaypoint : MonoBehaviour {

    public int WaypointID = 0;

    public Transform NextWaypoint;
    public Transform PreviousWaypoint;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawCube(this.transform.position, new Vector3(0.2f,0.2f,0.2f));
    }

    void Start()
    {
        if (NextWaypoint == null)
            NextWaypoint = this.transform;
        if (PreviousWaypoint == null)
            PreviousWaypoint = this.transform;
    }

    void OnTriggerStay(Collider _col)
    {
        //Debug.Log( "Collider " + _col.GetType() +"Caple Collider got hit " + _col.transform.name + " i am " + this.transform.name + " pos " + this.transform.position);

        if (_col.GetType() == typeof(CapsuleCollider))
        {
            CDefaultEnemy Unit = _col.transform.GetComponent<CDefaultEnemy>();

            if (Unit.b_forward == true && Unit.CurrentWaypoint == this.transform.position)
            {
                //Debug.Log("Inside if ++ forward == " + Unit.b_forward);
                if (_col.transform.tag == "Enemy")
                {
                    if (Unit.CurrentWaypoint == NextWaypoint.position && Unit.CurrentWaypoint == this.transform.position)
                    {
                        Unit.b_forward = false;
                    }
                    else if (Unit.CurrentWaypoint != NextWaypoint.position )
                    {
                        Unit.CurrentWaypoint = NextWaypoint.position;
                    }
                }
            }

            if (Unit.b_forward == false && Unit.CurrentWaypoint == this.transform.position)
            {
                if (_col.transform.tag == "Enemy")
                {
                    if (Unit.CurrentWaypoint == PreviousWaypoint.position && Unit.CurrentWaypoint == this.transform.position)
                    {
                        Unit.Die(false);
                    }
                    else if (Unit.CurrentWaypoint != PreviousWaypoint.position )
                    {
                        Unit.CurrentWaypoint = PreviousWaypoint.position;
                    }
                }
            }
        }
    }
}
