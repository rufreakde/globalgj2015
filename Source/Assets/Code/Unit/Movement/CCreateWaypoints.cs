﻿using UnityEngine;
using System.Collections;


public class CCreateWaypoints : MonoBehaviour 
{
    public Transform NextWaypoint;
    public Transform PreviousWaypoint;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawCube(this.transform.position, new Vector3(0.2f, 0.2f, 0.2f));
        Gizmos.DrawLine(this.transform.position, this.transform.position + Vector3.down * 8);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(NextWaypoint.position, this.transform.position);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(PreviousWaypoint.position + Vector3.up, this.transform.position + Vector3.up);
    }
}
