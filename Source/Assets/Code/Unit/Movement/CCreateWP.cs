﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CCreateWP : MonoBehaviour {

    //CGameManager GameManager;
    public GameObject WaypointPrefab;
    public RaycastHit hit;
    public LayerMask HitLayer;

    public Transform[] CreateWPList;
    public IList<CWaypoint> WPList = new List<CWaypoint>();
	// Use this for initialization

	void Start () 
    {
        //GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        foreach (Transform trans in CreateWPList)
        {
            Physics.Raycast(trans.position, Vector3.down, out hit, 8.0f , HitLayer);
            GameObject temp = Instantiate(WaypointPrefab, hit.point + Vector3.up * 0.2f, Quaternion.identity) as GameObject;
            temp.tag = "WayPoint";
            temp.name = trans.name;
            temp.GetComponent<BoxCollider>().enabled = false;
            WPList.Add(temp.transform.GetComponent<CWaypoint>());

            Destroy(this.gameObject);
        }

        SetPrevAndNext();
	}


    void SetPrevAndNext()
    { 
        for(int i=0; i<WPList.Count; i++)
        {
            if(i < WPList.Count - 1)
                WPList[i].NextWaypoint = WPList[i + 1].transform;
            else
                WPList[i].NextWaypoint = WPList[i].transform;

            if (i > 0)
                WPList[i].PreviousWaypoint = WPList[i - 1].transform;
            else
                WPList[i].PreviousWaypoint = WPList[i].transform;
        }
    }
}
