﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CBomb : MonoBehaviour
{
    protected CGameManager GameManager;
    protected CBombDamageRadius BombDamageRadius;
    public string s_Tag = "Enemy";
    public float f_time = 10.0f;
    public int i_damage = 10;
	
    void Start()
    {
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        BombDamageRadius = this.transform.parent.GetComponent<CBombDamageRadius>();
        StartCoroutine(Blinking(f_time));
    }

    // Apply damage to all enemies within range, the nearest gets the most damage
    void MakeDamage(List<GameObject> _enemyList)
    {
        if(_enemyList.Count > 0)
        {
            for (int i_Counter = _enemyList.Count - 1; i_Counter >= 0; i_Counter--)
            {
                CDefaultEnemy temp = _enemyList[i_Counter].transform.GetComponent<CDefaultEnemy>();
                int i_calculatedDamage = ((int)Mathf.Round(BombDamageRadius.GetTriggerRadius() - Vector3.Distance(this.transform.position, _enemyList[i_Counter].transform.position)) + i_damage) * GameManager.GameData.PlayerDat.i_Damage;
                if (i_calculatedDamage <= 0)
                {
                    i_calculatedDamage = 1;
                }

                temp.GetDamage(i_calculatedDamage);
            }
        }
    }

    IEnumerator Blinking(float _time)
    {
        float f_counter = _time;
        float f_color = 0.0f;
        bool b_isRed = false;
        Renderer r_temp = this.transform.renderer;

        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;
            f_color += GameManager.f_CustomDeltaTime;

            if (f_color >= 0.1f)
            {
                if (b_isRed)
                    r_temp.material.SetColor("_Color", new Color(255.0f, 255.0f, 255.0f));
                else
                    r_temp.material.SetColor("_Color", Color.white);

                if (b_isRed)
                    b_isRed = false;
                else
                    b_isRed = true;
                f_color = 0.0f;

            }

            if(!this.transform.FindChild("Explosion").gameObject.activeSelf && f_counter <= 0.5f)
            {
                this.transform.FindChild("Explosion").gameObject.SetActive(true);
            }

            yield return new WaitForEndOfFrame();
        }

        r_temp.material.SetColor("_Color", Color.white);
        MakeDamage(BombDamageRadius.GetEnemiesInRadius());
        Destroy(this.transform.parent.gameObject);
    }

    // This will make the bomb explode on collision
    void CollisionExplode()
    {
        StartCoroutine(Blinking(0.5f));
    }

    void OnCollisionEnter(Collision _target)
    {
        if(_target.gameObject.tag == s_Tag)
        {
            CollisionExplode();
        }
    }
}
