﻿using UnityEngine;
using System.Collections;

public class CDamage : MonoBehaviour
{
    protected CGameManager GameManager;

    void OnEnable()
    {
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, this.GetComponent<SphereCollider>().radius);
    }

    void OnTriggerEnter(Collider _target)
    {
        if (_target.GetType() == typeof(CapsuleCollider))
        {
            CPlayer Player = _target.GetComponent<CPlayer>();
            CDefaultEnemy DefaultEnemy = _target.GetComponent<CDefaultEnemy>();

            if (_target.transform.tag == "Player")
            {
                Player.GetDamage(DefaultEnemy.i_AttackDamage);
            }
            else if (_target.transform.tag == "Enemy")
            {
                DefaultEnemy.GetDamage(GameManager.GameData.PlayerDat.i_Damage);
            }

            Destroy(this.gameObject);
        }
    }
}
