﻿using UnityEngine;
using System.Collections;

public class DontDestroyOnLoad : MonoBehaviour {

    public GameObject MainMenu;
    public GameObject Options;
    public GameObject AreYouSure;
    public GameObject DataManager;
    public GameObject Highscore;

	// Use this for initialization
    public static DontDestroyOnLoad SMenu;

    void Awake()
    {
        if (!SMenu)
        {
            SMenu = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void OnLevelWasLoaded(int _LevelID)
    {
        if( _LevelID == 0)
        {
            MainMenu.SetActive(false);
            Options.SetActive(false);
            AreYouSure.SetActive(false);
            DataManager.SetActive(true);
            Highscore.SetActive(true);
        }
        else if(_LevelID == 1)
        {
            MainMenu.SetActive(false);
            Options.SetActive(false);
            AreYouSure.SetActive(false);
            DataManager.SetActive(true);
            Highscore.SetActive(false);
        }
    }
}
