﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CAttributes : MonoBehaviour
{
    public int i_Attribute = 0;
    CGameManager GameManager;
    Image AttributeBar;
    float widthMax = 410f;

    // Use this for initialization
    void Start()
    {
        if (Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        AttributeBar = transform.GetComponent<Image>();

        widthMax = AttributeBar.rectTransform.rect.width;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager == null && Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();

        AttributeBar.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 1.0f, widthMax * ReturnScale());
    }

    float ReturnScale()
    {
        switch(i_Attribute)
        {
            case 0:
                return GameManager.GameData.PlayerDat.i_SkilledDamage / 50.0f;

            case 1:
                return GameManager.GameData.PlayerDat.i_SkilledSpeed / 50.0f;

            case 2:
                return GameManager.GameData.PlayerDat.i_SkilledHealth / 50.0f;

            default:
                return GameManager.GameData.PlayerDat.i_SkilledDamage / 50.0f;
        }
    }
}
