﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CGameData))]
public class CGameManager : MonoBehaviour {

    public GameObject   UpgradeUI;
    public CGameData    GameData;
    public CDifficulty  TheDifficulty;
    public CHighScore   TheHighscore;
    public GameObject   DefaultEnemyPrefab1;
    public GameObject   DefaultEnemyPrefab2;
    public AudioClip Parrot_1, Parrot_2;
    private AudioSource ParrotAudio;
    private float ParrotTimer;

    public CPlayerPrefsShow UpdateTextOfPlayerPrefs;
    public GameObject HighscoreObject;

    public float f_MinSpawnDistance = 15.0f;
    public float f_MaxSpawnDistance = 25.0f;
    public float f_CustomDeltaTime = 0.0f;
    public int i_EnemyCount = 3;

    public int i_Wave = 0;
    public bool b_Pause = false;

    //Calculate gametime for pause
    void Start()
    {
        GameData = transform.GetComponent<CGameData>();
        TheHighscore = GameObject.Find("DataManager").GetComponent<CHighScore>();
        HighscoreObject = GameObject.Find("Highscore");
        ParrotAudio = GameObject.Find("Map").GetComponent<AudioSource>();
    }

    //Calulace custom game to for pause
    float CalcGameTime(float _UnityDeltaTime, bool _Stop)
    {
        if (!_Stop)
            return _UnityDeltaTime;
        else
            return 0.0f;
    }

    public void NextWave()
    {
        Debug.Log(this.GameData.GameDat.i_EnemyCount + " Enemies left!");

        if (this.GameData.GameDat.i_EnemyCount <= 0)
        {
            Debug.Log("Next Wave!");

            //spiel pausieren
            Pause(true);

            //upgrade menu zeigen
            if(!UpgradeUI.activeSelf)
                ToggleShowUpgradeMenu();

            //calc Difficulty
            //this will be handled automaticaly by "CDifficulty"

            //Wave starten  
            //gegener spawnen
            i_Wave++;
            StartWave(TheDifficulty);
            

            //spiel geht weiter sobald das upgrade gewählt wurde.
            HighscoreObject = GameObject.Find("Menu").transform.FindChild("Highscore").gameObject;

            if (UpdateTextOfPlayerPrefs == null)
                UpdateTextOfPlayerPrefs = HighscoreObject.GetComponent<CPlayerPrefsShow>();
        }

    }

    void StartWave(CDifficulty _TheDifficulty)
    {
        if((float)i_Wave % 5.0f == 0)
        {
            i_EnemyCount++;

            if (GameData.PlayerDat.i_Bombs + 1 <= GameData.PlayerDat.i_MaxBombs)
                GameData.PlayerDat.i_Bombs++;
        }

        SpawnEnemies();
        //Wave starten  
        //gegener spawnen

    }

    void SpawnEnemies()
    {
        int i_Random = 0;
        Vector3 v_spawnPoint = new Vector3();

        for (int i_Counter = 1; i_Counter <= i_EnemyCount; i_Counter++)
        {
            i_Random = Random.Range(0, 4);

            switch(i_Random)
            {
                case 0:
                    v_spawnPoint = new Vector3(Random.Range(f_MinSpawnDistance, f_MaxSpawnDistance), 0.0f, Random.Range(f_MinSpawnDistance, f_MaxSpawnDistance));
                    break;

                case 1:
                    v_spawnPoint = new Vector3(Random.Range(-f_MaxSpawnDistance, -f_MinSpawnDistance), 0.0f, Random.Range(f_MinSpawnDistance, f_MaxSpawnDistance));
                    break;

                case 2:
                    v_spawnPoint = new Vector3(Random.Range(f_MinSpawnDistance, f_MaxSpawnDistance), 0.0f, Random.Range(-f_MaxSpawnDistance, -f_MinSpawnDistance));
                    break;

                case 3:
                    v_spawnPoint = new Vector3(Random.Range(-f_MaxSpawnDistance, -f_MinSpawnDistance), 0.0f, Random.Range(-f_MaxSpawnDistance, -f_MinSpawnDistance));
                    break;

                default:
                    v_spawnPoint = new Vector3(Random.Range(f_MinSpawnDistance, f_MaxSpawnDistance), 0.0f, Random.Range(f_MinSpawnDistance, f_MaxSpawnDistance));
                    break;
            }

            if(i_Random == 0 || i_Random == 2)
                Instantiate(DefaultEnemyPrefab1, v_spawnPoint, Quaternion.identity);
            else
                Instantiate(DefaultEnemyPrefab2, v_spawnPoint, Quaternion.identity);

            GameData.GameDat.i_EnemyCount++;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(GameObject.FindGameObjectWithTag("Gold").transform.position, f_MinSpawnDistance);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(GameObject.FindGameObjectWithTag("Gold").transform.position, f_MaxSpawnDistance);
    }

    public void CheckGameStatus()
    {
        if (GameData.PlayerDat.i_Health <= 0 || GameData.GameDat.i_Gold <= 0)
        {
            if (TheHighscore == null)
                TheHighscore = GameObject.Find("DataManager").GetComponent<CHighScore>();

            if (HighscoreObject == null)
                HighscoreObject = GameObject.Find("Highscore");

            if (UpdateTextOfPlayerPrefs == null)
                UpdateTextOfPlayerPrefs = HighscoreObject.GetComponent<CPlayerPrefsShow>();

            //Calculate the Highscore
            TheHighscore.CalculateHighscore();

            //show highscore
            //ShowHighscore(TheHighscore);

            //GameOver
            GameOver();
        }
    }

    protected void GameOver()
    {
        HighscoreObject.SetActive(true);
        //TheHighscore.CalculateHighscore();
        TheHighscore.OpenHighscore();
        UpdateTextOfPlayerPrefs.ChangePrefs();

        Application.LoadLevel("Menu");
    }

    protected void ShowHighscore(CHighScore _Score)
    { 

        //get score and show in ui :)
        _Score.OpenHighscore();

    }

    public void Pause(bool _OnOff)
    {
        b_Pause = _OnOff;
    }

    public bool ToggleShowUpgradeMenu()
    {
        if (!UpgradeUI.activeSelf)
        {
            UpgradeUI.SetActive(true);
            return true;
        }
        else
        {
            UpgradeUI.SetActive(false);
            return false;
        }
    }

    public void Upgrade(PlayerUpgrades _Upgrade ,  int Value)
    { 
        switch(_Upgrade)
        {
            case PlayerUpgrades.Damage: 

                break;

            case PlayerUpgrades.Health:

                break;

            case PlayerUpgrades.Speed:

                break;
        }
    }

    public void UpgradeHealth()
    {
        GameData.PlayerDat.i_HealthMax += GameData.PlayerDat.i_HealthAddition;
        GameData.PlayerDat.i_Health += GameData.PlayerDat.i_HealthAddition;
        GameData.PlayerDat.i_SkilledHealth++;

        GameData.PlayerDat.i_Health += (int)(((float)GameData.PlayerDat.i_HealthMax / 100.0f) * 20.0f);
        if (GameData.PlayerDat.i_Health > GameData.PlayerDat.i_HealthMax)
            GameData.PlayerDat.i_Health = GameData.PlayerDat.i_HealthMax;

        ToggleShowUpgradeMenu();
        Pause(false);
    }

    public void UpgradeDamage()
    {
        GameData.PlayerDat.i_Damage += GameData.PlayerDat.i_DamageAddition;
        GameData.PlayerDat.i_SkilledDamage++;

        GameData.PlayerDat.i_Health += (int)(((float)GameData.PlayerDat.i_HealthMax / 100.0f) * 20.0f);
        if (GameData.PlayerDat.i_Health > GameData.PlayerDat.i_HealthMax)
            GameData.PlayerDat.i_Health = GameData.PlayerDat.i_HealthMax;

        ToggleShowUpgradeMenu();
        Pause(false);
    }

    public void UpgradeSpeed()
    {
        GameData.PlayerDat.f_Speed += GameData.PlayerDat.f_SpeedAddition;
        GameData.PlayerDat.i_SkilledSpeed++;

        GameData.PlayerDat.i_Health += (int)(((float)GameData.PlayerDat.i_HealthMax / 100.0f) * 20.0f);
        if (GameData.PlayerDat.i_Health > GameData.PlayerDat.i_HealthMax)
            GameData.PlayerDat.i_Health = GameData.PlayerDat.i_HealthMax;

        ToggleShowUpgradeMenu();
        Pause(false);
    }

	void Update () 
    {
        if (!GameData.PlayerDat.b_IsDead && !b_Pause)
        {
            f_CustomDeltaTime = CalcGameTime(Time.deltaTime, b_Pause);
        }
        else
        {
            f_CustomDeltaTime = 0.0f;

        }
            NextWave();


        if(ParrotTimer <= 0f)
        {
            if(Random.Range(0, 2) < 1)
            {
                ParrotAudio.clip = Parrot_1;
                ParrotAudio.Play();
            }
            else
            {
                ParrotAudio.clip = Parrot_2;
                ParrotAudio.Play();
            }

            ParrotTimer = Random.Range(8.0f, 24.0f);
        }
        else
        {
            ParrotTimer -= Time.deltaTime;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("Menu");
        }
	}
}
