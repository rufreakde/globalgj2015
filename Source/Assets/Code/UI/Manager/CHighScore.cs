﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CHighScore : MonoBehaviour 
{
    public GameObject Highscore;
    public GameObject MainMenu;
    public CGameManager GameManager;
    protected int i_highscore;
    public int CurrentScore = 0;
    public float _currentScore = 0.0f;
    int[] Score = new int[5];
    int[] Gold = new int[5];
    int[] Time = new int[5];

    public void OnEnable()
    {
        if (Application.loadedLevelName.Contains("Game"))
        {
            Debug.Log("init GameManager");
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        }
        _currentScore = 0f;
    }

    public void Start()
    {
        if (PlayerPrefs.GetInt("1_p") == 0)
        {
            Debug.LogWarning("Init Playerprefs");
            InitPlayerPrefs();
        }
    }

    public void InitPlayerPrefs()
    {
        Debug.LogWarning("Init PF" );
        PlayerPrefs.SetInt("1_p", 50);
        PlayerPrefs.SetInt("2_p", 40);
        PlayerPrefs.SetInt("3_p", 30);
        PlayerPrefs.SetInt("4_p", 20);
        PlayerPrefs.SetInt("5_p", 10);

        PlayerPrefs.SetInt("1_g", 555);
        PlayerPrefs.SetInt("2_g", 444);
        PlayerPrefs.SetInt("3_g", 333);
        PlayerPrefs.SetInt("4_g", 222);
        PlayerPrefs.SetInt("5_g", 111);

        PlayerPrefs.SetInt("1_t", 555);
        PlayerPrefs.SetInt("2_t", 444);
        PlayerPrefs.SetInt("3_t", 333);
        PlayerPrefs.SetInt("4_t", 222);
        PlayerPrefs.SetInt("5_t", 111);
    }

    public void OpenHighscore()
    {
        Highscore.SetActive(true);
        MainMenu.SetActive(false);
    }

    public void Close()
    {
        Highscore.SetActive(false);
        MainMenu.SetActive(true);
        if (Application.loadedLevelName != "Menu")
        {
            Application.LoadLevel("Menu");
        }
    }

    void Update()
    {
        if (Application.loadedLevelName.Contains("Game"))
        {
            if (GameManager == null)
            {
                GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
            }
            _currentScore += GameManager.f_CustomDeltaTime * 3f;
            //Debug.Log(_currentScore);
        }
    }

    public List<int> CalculateHighscore()
    {

        CurrentScore = ((int)_currentScore + GameManager.GameData.GameDat.i_Gold);
        int newTime = (int)((_currentScore) / 3f);
        int newGold = GameManager.GameData.GameDat.i_Gold;

        List<int> NewScore = new List<int>();
        List<int> arNewGold = new List<int>();
        List<int> arNewTime = new List<int>();

        Score[0] = PlayerPrefs.GetInt("1_p");
        Score[1] = PlayerPrefs.GetInt("2_p");
        Score[2] = PlayerPrefs.GetInt("3_p");
        Score[3] = PlayerPrefs.GetInt("4_p");
        Score[4] = PlayerPrefs.GetInt("5_p");

        Gold[0] = PlayerPrefs.GetInt("1_g");
        Gold[1] = PlayerPrefs.GetInt("2_g");
        Gold[2] = PlayerPrefs.GetInt("3_g");
        Gold[3] = PlayerPrefs.GetInt("4_g");
        Gold[4] = PlayerPrefs.GetInt("5_g");

        Time[0] = PlayerPrefs.GetInt("1_t");
        Time[1] = PlayerPrefs.GetInt("2_t");
        Time[2] = PlayerPrefs.GetInt("3_t");
        Time[3] = PlayerPrefs.GetInt("4_t");
        Time[4] = PlayerPrefs.GetInt("5_t");

        int index = 0;
        for (int i = 0; i < Score.Length; i++ )
        {
            if (i == Score.Length - 1)
            {
                Debug.Log("Best Score! " + Score.Length);
                index = 0;
                break;
            }
            if (Score[i] < CurrentScore && Score[i + 1] >= CurrentScore)
            {
                index = i;
                Debug.Log(" INDEX " + i);
                break;
            }
        }

        NewScore.AddRange(Score);
        NewScore.Insert(index, CurrentScore);
        for (int i = 0; i < NewScore.Count; i++)
        {
            Debug.Log(NewScore[i] + "  NewScore " + i);
        }

        arNewGold.AddRange(Gold);
        arNewGold.Insert(index, newGold);
        for (int i = 0; i < NewScore.Count; i++)
        {
            Debug.Log(arNewGold[i] + "  NewGolf " + i);
        }

        arNewTime.AddRange(Time);
        arNewTime.Insert(index, newTime);
        for (int i = 0; i < NewScore.Count; i++)
        {
            Debug.Log(arNewTime[i] + "  NewTime " + i);
        }

        PlayerPrefs.SetInt("1_p", NewScore[0]);
        PlayerPrefs.SetInt("2_p", NewScore[1]);
        PlayerPrefs.SetInt("3_p", NewScore[2]);
        PlayerPrefs.SetInt("4_p", NewScore[3]);
        PlayerPrefs.SetInt("5_p", NewScore[4]);

        PlayerPrefs.SetInt("1_g", arNewGold[0]);
        PlayerPrefs.SetInt("2_g", arNewGold[1]);
        PlayerPrefs.SetInt("3_g", arNewGold[2]);
        PlayerPrefs.SetInt("4_g", arNewGold[3]);
        PlayerPrefs.SetInt("5_g", arNewGold[4]);

        PlayerPrefs.SetInt("1_t", arNewTime[0]);
        PlayerPrefs.SetInt("2_t", arNewTime[1]);
        PlayerPrefs.SetInt("3_t", arNewTime[2]);
        PlayerPrefs.SetInt("4_t", arNewTime[3]);
        PlayerPrefs.SetInt("5_t", arNewTime[4]);
        return NewScore;
    }
}
