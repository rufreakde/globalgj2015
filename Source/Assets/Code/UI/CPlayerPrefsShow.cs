﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CPlayerPrefsShow : MonoBehaviour {

    int[] Score = new int[5];
    int[] Gold = new int[5];
    int[] Time = new int[5];

    public Text PunkteText;
    public Text GoldText;
    public Text TimeText;

    public void ChangePrefs()
    {
        Score[0] = PlayerPrefs.GetInt("1_p");
        Score[1] = PlayerPrefs.GetInt("2_p");
        Score[2] = PlayerPrefs.GetInt("3_p");
        Score[3] = PlayerPrefs.GetInt("4_p");
        Score[4] = PlayerPrefs.GetInt("5_p");

        Gold[0] = PlayerPrefs.GetInt("1_g");
        Gold[1] = PlayerPrefs.GetInt("2_g");
        Gold[2] = PlayerPrefs.GetInt("3_g");
        Gold[3] = PlayerPrefs.GetInt("4_g");
        Gold[4] = PlayerPrefs.GetInt("5_g");

        Time[0] = PlayerPrefs.GetInt("1_t");
        Time[1] = PlayerPrefs.GetInt("2_t");
        Time[2] = PlayerPrefs.GetInt("3_t");
        Time[3] = PlayerPrefs.GetInt("4_t");
        Time[4] = PlayerPrefs.GetInt("5_t");

        TimeText.text = "";
        GoldText.text = "";
        PunkteText.text = "";

        for (int i = 0; i < Score.Length; i++)
        {
            Debug.Log("Draw the UI Text " + i);
            TimeText.text += ((float)Time[i]/60f).ToString(".##") + "\n";
            GoldText.text += Gold[i].ToString() + "\n";
            PunkteText.text += Score[i].ToString() + "\n";
        }
    }
}
