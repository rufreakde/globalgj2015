﻿using UnityEngine;
using System.Collections;

public class DeactivateInScene : MonoBehaviour {

	
	// Update is called once per frame
	void Update () 
    {
        if (Application.loadedLevelName == "Menu")
            this.transform.gameObject.SetActive(false);
	}
}
