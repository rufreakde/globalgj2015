﻿using UnityEngine;
using System.Collections;

public class CExitGame : MonoBehaviour {

    public GameObject AreYouSureUI;
    public GameObject MainMenu;
    public GameObject IngameMenu;

    public void ExitGame()
    {
        AreYouSureUI.SetActive(true);
    }

    public void Yes()
    {
        Application.Quit();
    }

    public void No()
    {
        AreYouSureUI.SetActive(false);
    }

    public void StartGame()
    {
        MainMenu.SetActive(false);
        IngameMenu.SetActive(true);
        Application.LoadLevel("Game");
    }
}
