﻿using UnityEngine;
using System.Collections;

public class COptions : MonoBehaviour {

    public static bool SoundOnOff = true;
    public static bool SfxOnOff = true;

    public static COptions SOptions;
    public GameObject go_Options;
    public GameObject go_MainMenu;
    public GameObject go_credits;

    void Awake()
    {
        if (!SOptions)
        {
            SOptions = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void Close()
    {
        go_Options.SetActive(false);
        go_MainMenu.SetActive(true);
    }

    public void CloseCredits()
    {
        go_credits.SetActive(false);
        go_MainMenu.SetActive(true);
    }

    public void ToggleSound()
    {
        if (SoundOnOff)
            SoundOnOff = false;
        else
            SoundOnOff = true;
    }

    public void OpenOptions()
    {
        go_Options.SetActive(true);
        go_MainMenu.SetActive(false);    
    }

    public void OpenCredits()
    {
        go_credits.SetActive(true);
        go_MainMenu.SetActive(false);
    }

    public void ToggleSfc()
    {
        if (SfxOnOff)
            SfxOnOff = false;
        else
            SfxOnOff = true;
    }
}
