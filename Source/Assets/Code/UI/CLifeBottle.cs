﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CLifeBottle : MonoBehaviour {

    CGameManager GameManager;
    Image RedRum;
    float heightMax = 773f;

	// Use this for initialization
	void OnEnable() 
    {
        if (Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        RedRum = transform.GetComponent<Image>();

        heightMax = RedRum.rectTransform.rect.height;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (GameManager == null && Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();

        //Rect aRect = RedRum.rectTransform.rect;
       // aRect.height = (heightMax * ReturnLifeScale());
        //Debug.Log(aRect.height);
        RedRum.rectTransform.SetInsetAndSizeFromParentEdge( RectTransform.Edge.Bottom, 1.0f, heightMax * ReturnLifeScale());
	}

    float ReturnLifeScale()
    {
        float LifePercent = (((float)GameManager.GameData.PlayerDat.i_Health) / (float)GameManager.GameData.PlayerDat.i_HealthMax);
        return LifePercent;
    }
}
