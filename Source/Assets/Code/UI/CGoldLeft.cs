﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CGoldLeft : MonoBehaviour {

    CGameManager GameManager;
    Text Gold;

    // Use this for initialization
    void Start()
    {
        if (Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        Gold = transform.GetComponent<Text>();

        Gold.text = "1000";
    }

    void Update()
    {
        if (GameManager == null && Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();

        if (GameManager != null)
            Gold.text = GameManager.GameData.GameDat.i_Gold.ToString("0") + "/" + GameManager.GameData.GameDat.i_MaximumGold;
    }
}