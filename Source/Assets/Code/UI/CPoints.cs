﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CPoints : MonoBehaviour {

    CGameManager GameManager;
    Text Points;

    // Use this for initialization
    void Start()
    {
        if (Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        Points = transform.GetComponent<Text>();

        Points.text = "0";
    }

    void Update()
    {
        if (GameManager == null && Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();

        if( GameManager != null)
            Points.text = GameManager.TheHighscore._currentScore.ToString("0");
    }
}
