﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CEnemiesCount : MonoBehaviour {

    CGameManager GameManager;
    Text EnemyCount;

    // Use this for initialization
    void Start()
    {
        if (Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        EnemyCount = transform.GetComponent<Text>();

        EnemyCount.text = "0";
    }

    void Update()
    {
        if (GameManager == null && Application.loadedLevelName.Contains("Game"))
            GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();

        if (GameManager != null)
            EnemyCount.text = GameManager.GameData.GameDat.i_EnemyCount.ToString();
    }
}