﻿using UnityEngine;
using System.Collections;

public class CSoundCheck : MonoBehaviour
{
    protected CGameManager GameManager;

    public bool sfx = true;
    public bool sound = false;

	// Use this for initialization
	void Start ()
    {
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        if (sfx)
        {
            if (COptions.SfxOnOff)
            {
                this.audio.Play();
            }
        }

        if (sound)
        {
            if (COptions.SoundOnOff)
            {
                this.audio.Play();
            }
        }
	}
}
