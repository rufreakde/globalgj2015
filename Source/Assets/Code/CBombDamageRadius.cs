﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CBombDamageRadius : MonoBehaviour
{
    public List<GameObject> go_enemyList = new List<GameObject>();

    public List<GameObject> GetEnemiesInRadius()
    {
        return go_enemyList;
    }

    public float GetTriggerRadius()
    {
        return this.GetComponent<SphereCollider>().radius;
    }

    void OnTriggerEnter(Collider _target)
    {
        if(_target.tag == "Enemy")
        {
            go_enemyList.Add(_target.gameObject);
        }
    }

    void OnTriggerExit(Collider _target)
    {
        if(go_enemyList.Contains(_target.gameObject))
        {
            go_enemyList.Remove(_target.gameObject);
        }
    }
}
