﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CDefaultEnemy : MonoBehaviour
{
    protected CGameManager GameManager;
    protected CPlayer Player;
    public bool b_forward = true;
    public string s_MeshName = "Mesh";
    public Color c_DamageColor = new Color(1.0f, 0.1f, 0.1f, 0.75f);
    public float f_DamageBlinkTime = 1.5f;
    public float f_DamageBlinkSpeed = 0.2f;
    public int i_EnemyHealth = 20;
    public float f_DyingTime = 1.0f;
    public int i_AttackDamage = 1;
    public float f_AttackRange = 0.8f;
    public float f_AttackSpeed = 1.5f;
    public float f_MovementSpeed = 0.8f;
    public float f_GoldStealPercentage = 5.0f;
    public float f_DestinationTol = 0.3f;
    protected int i_WayPointID = 0;
    protected Animator a_MyAnimator;

    public Vector3 CurrentWaypoint;
    //use later to switch between targets!
    public Vector3 AttackWaypoint;

    protected int i_GoldStealAmount = 0;
    protected bool b_isFighting = false;
    protected bool b_isAttacking = false;
    protected bool b_stoleGold = false;
    protected bool b_isDead = false;

    public bool b_IstoleSomething = false;

	// Use this for initialization
	void Start()
    {
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<CPlayer>();

        a_MyAnimator = transform.FindChild("Mesh").GetComponent<Animator>();
        i_GoldStealAmount = (int)(((float)GameManager.GameData.GameDat.i_MaximumGold / 100.0f) * f_GoldStealPercentage);

        //create wapoint chain
        GetNearestWaypoint();

        //Evaluate Game
        i_EnemyHealth += GameManager.TheDifficulty.i_HealthAddition * (GameManager.i_Wave / 10);
        i_AttackDamage += GameManager.TheDifficulty.i_DamageAddition * (GameManager.i_Wave / 10);
        f_MovementSpeed += GameManager.TheDifficulty.f_SpeedAddition * (GameManager.i_Wave / 10);
        f_AttackRange += GameManager.TheDifficulty.f_AttackRangeAddition * (GameManager.i_Wave / 10);
        f_AttackSpeed += GameManager.TheDifficulty.f_AttackSpeedAddition * (GameManager.i_Wave / 10);
    }
	
	// Update is called once per frame
	void Update()
    {
        if (!GameManager.GameData.PlayerDat.b_IsDead && !GameManager.b_Pause && !b_isDead)
        {
            AmIDead();
            Move();
            Fight();
        }
	}

    void Move()
    {
        if(b_isFighting)
        {
            AttackWaypoint = Player.transform.position;

            if(Vector3.Distance(transform.position, Player.transform.position) > f_AttackRange)
            {
                rigidbody.velocity = (AttackWaypoint - transform.position).normalized * f_MovementSpeed;
                rigidbody.velocity += Vector3.down * 0.1f;
            }

            this.transform.LookAt(new Vector3(AttackWaypoint.x, transform.position.y, AttackWaypoint.z));
        }
        else
        {
                rigidbody.velocity = (CurrentWaypoint - transform.position).normalized * f_MovementSpeed;
                rigidbody.velocity += Vector3.down * 0.1f;

                this.transform.LookAt(new Vector3(CurrentWaypoint.x, transform.position.y, CurrentWaypoint.z));
        }
    }

    void Fight()
    {
        if (!b_isAttacking && b_isFighting && Vector3.Distance(this.transform.position, Player.transform.position) <= f_AttackRange)
        {
            //Debug.Log("Let the fight begins!");
            b_isAttacking = true;
            StartCoroutine(Attacking(f_AttackSpeed));
        }
    }

    void AmIDead()
    {
        if(i_EnemyHealth <= 0)
        {
            a_MyAnimator.SetBool("Dead", true);
            Die(true);
        }
    }

    public void Steal()
    {
        //Debug.Log("Oh no!" + i_GoldStealAmount + "Gold were stolen!");
        a_MyAnimator.SetTrigger("Steal");
        GameManager.GameData.GameDat.i_Gold -= i_GoldStealAmount;
        b_stoleGold = true;
    }

    public void GetDamage(int _damage)
    {
        StartCoroutine(Blinking(f_DamageBlinkTime));
        i_EnemyHealth -= _damage;
    }

    void DropGold()
    {
        if(b_stoleGold)
        {
            GameManager.GameData.GameDat.i_Gold += i_GoldStealAmount;
        }
    }

    Vector3 GetNearestWaypoint()
    {
        GameObject[] Array = GameObject.FindGameObjectsWithTag("WayPoint");

        float Distance = 999999999999999f;

        for (int i = 0; i < Array.Length; i++)
        { 
            if(Vector3.Distance(Array[i].transform.position, this.transform.position) < Distance)
            {
                Distance = Vector3.Distance(Array[i].transform.position, this.transform.position);
                CurrentWaypoint = Array[i].transform.position;
            }
        }
        return CurrentWaypoint;
    }

    IEnumerator Blinking(float _time)
    {
        float f_counter = _time;
        float f_color = 0.0f;
        bool b_isRed = false;
        Renderer r_temp = this.transform.FindChild(s_MeshName).FindChild("Cube").renderer;

        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;
            f_color += GameManager.f_CustomDeltaTime;

            if (f_color >= f_DamageBlinkSpeed)
            {
                if(b_isRed)
                    r_temp.material.SetColor("_Color", c_DamageColor);
                else
                    r_temp.material.SetColor("_Color", Color.white);

                if(b_isRed)
                    b_isRed = false;
                else
                    b_isRed = true;
                f_color = 0.0f;
                
            }
            yield return new WaitForEndOfFrame();
        }
        r_temp.material.SetColor("_Color", Color.white);
    }

    public void Die(bool _killedByPlayer)
    {
        b_isDead = true;
        this.transform.rigidbody.Sleep();

        if (_killedByPlayer)
            StartCoroutine(Dying(f_DyingTime));
        else
            StartCoroutine(DyingWithoutDrop(f_DyingTime));
    }

    IEnumerator Dying(float _animationTime)
    {
        float f_counter = _animationTime;

        //Death animation playback
        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;

            if (!this.transform.FindChild("Die").gameObject.activeSelf && f_counter <= 1.0f)
            {
                this.transform.FindChild("Die").gameObject.SetActive(true);
            }

            yield return new WaitForEndOfFrame();
        }

        DropGold();
        GameManager.GameData.GameDat.i_EnemyCount--;
        Destroy(this.gameObject);
    }

    IEnumerator DyingWithoutDrop(float _animationTime)
    {
        float f_counter = _animationTime;

        //Death animation playback
        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;

            if (!this.transform.FindChild("Die").gameObject.activeSelf && f_counter <= 1.0f)
            {
                this.transform.FindChild("Die").gameObject.SetActive(true);
            }

            yield return new WaitForEndOfFrame();
        }

        GameManager.GameData.GameDat.i_EnemyCount--;
        Destroy(this.gameObject);
    }

    IEnumerator Attacking(float _animationTime)
    {
        float f_counter = _animationTime;

        a_MyAnimator.SetTrigger("Attack");
        this.audio.Play();

        //Attack animation playback
        while (f_counter >= 0f)
        {
            f_counter -= GameManager.f_CustomDeltaTime;

            yield return new WaitForEndOfFrame();
        }
        Player.GetDamage(i_AttackDamage);

        b_isAttacking = false;
    }

    void OnTriggerEnter(Collider _target)
    {
        if(_target.tag == "Player")
        {
            b_isFighting = true;
        }
    }

    void OnTriggerExit(Collider _target)
    {
        if (_target.tag == "Player")
        {
            b_isFighting = false;
        }
    }
}
