﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CToggleOff : MonoBehaviour {

    public Image Image;
    public bool SFX;

    void Start()
    {
        Image = transform.GetComponent<Image>();
    }
	// Update is called once per frame
	void Update () 
    {
        if (SFX)
        {
            if (COptions.SfxOnOff == false)
                Image.enabled = true;
            else
                Image.enabled = false;
        }
        else
            if (COptions.SoundOnOff == false)
                Image.enabled = true;
            else
                Image.enabled = false;
	}
}
