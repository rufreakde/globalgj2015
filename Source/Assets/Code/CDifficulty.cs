﻿using UnityEngine;
using System.Collections;

public class CDifficulty : MonoBehaviour
{
    protected CGameManager GameManager;
    protected int i_highscore;
    public int i_HealthAddition = 3;
    public int i_DamageAddition = 3;
    public float f_SpeedAddition = 0.1f;
    public float f_AttackRangeAddition = 0.0f;
    public float f_AttackSpeedAddition = 0.0f;

    void Start()
    {
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<CGameManager>();
    }

    void SetDifficulty(int _difficulty)
    {
        GameManager.GameData.GameDat.i_Difficulty = _difficulty;
    }

    public void EvalGame()
    {
    }
}
